from sqlalchemy import JSON, Column, Integer, String

from database import Base


class Recipe(Base):
    __tablename__ = 'Recipe'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    views = Column(Integer, index=True)
    cooking_time = Column(Integer, index=True)
    ingredients = Column(JSON, index=True)
    description = Column(String, index=True)
