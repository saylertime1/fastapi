import pytest
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from database import Base
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

TEST_DATABASE_URL = "sqlite+aiosqlite:///./fake.db"

engine = create_async_engine(TEST_DATABASE_URL, echo=True)
async_session = sessionmaker(engine,
                             expire_on_commit=False,
                             class_=AsyncSession)
session = async_session()


@pytest.fixture(autouse=True)
async def create_test_database():
    async with engine.begin() as connection:
        await connection.run_sync(Base.metadata.create_all)
    yield
    async with engine.begin() as connection:
        await connection.run_sync(Base.metadata.drop_all)


@pytest.mark.asyncio
async def test_post_recipe():
    test_data = {
        "name": "Тестовый пирожок",
        "views": 10,
        "cooking_time": 5,
        "ingredients": ["мука", "соль"],
        "description": "Простой тестовый пирожок без начинки"
    }
    response = client.post('/post_recipe', json=test_data)
    assert response.status_code == 200
    assert 'Тестовый пирожок' in response.text
    assert 'без начинки' in response.text


def test_recipes_200():
    response = client.get("/recipes")
    assert response.status_code == 200


def test_recipe_id_404():
    response_200 = client.get("/recipes/1")
    response_404 = client.get("/recipes/333")
    assert response_200.status_code == 200
    assert response_404.status_code == 404
