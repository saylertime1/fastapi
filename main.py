from typing import List

from sqlalchemy import desc
from sqlalchemy.future import select

import models
import schemas
from database import engine, session
from fastapi import FastAPI, HTTPException

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as connection:
        await connection.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.post("/post_recipe", response_model=schemas.RecipeOut)
async def post_recipe(recipe: schemas.RecipeIn):
    new_recipe = models.Recipe(**recipe.dict())
    async with session.begin():
        session.add(new_recipe)
    return new_recipe


@app.get("/recipes", response_model=List[schemas.OutAll])
async def all_recipes():
    result = await session.execute(
        select(models.Recipe).order_by(
            desc(models.Recipe.views), models.Recipe.cooking_time
        )
    )
    return result.scalars().all()


@app.get("/recipes/{id}", response_model=schemas.BaseRecipe)
async def get_recipe(id: int):
    recipe_filter = models.Recipe.id == id
    query = select(models.Recipe).filter(recipe_filter)
    result = await session.execute(query)

    recipe = result.scalars().first()

    if recipe:
        return {
            "name": recipe.name,
            "views": recipe.views,
            "cooking_time": recipe.cooking_time,
            "ingredients": recipe.ingredients,
            "description": recipe.description,
        }
    else:
        raise HTTPException(status_code=404, detail="Recipe not found")
