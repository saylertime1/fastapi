from pydantic import BaseModel


class BaseRecipe(BaseModel):
    name: str
    views: int
    cooking_time: int
    ingredients: list
    description: str


class RecipeIn(BaseRecipe):
    ...


class OutAll(BaseModel):
    name: str
    views: int
    cooking_time: int


class RecipeOut(BaseRecipe):
    id: int

    class ConfigDict:
        orm_mode = True
